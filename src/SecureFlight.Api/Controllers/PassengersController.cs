﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PassengersController : ControllerBase
    {
        private readonly IPassengerService _personService;

        public PassengersController(IPassengerService personService)
        {
            _personService = personService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<PassengerDataTransferObject>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
        public async Task<IActionResult> Get()
        {
            var passengers = (await _personService.GetAllAsync()).Result
                .Select(x => new PassengerDataTransferObject
                {
                    Email = x.Email,
                    FirstName = x.FirstName,
                    Id = x.Id,
                    LastName = x.LastName
                });

            return Ok(passengers);
        }

        [HttpPost("/{passengerId}/{flightId}")]
        [ProducesResponseType(typeof(IEnumerable<PassengerDataTransferObject>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErrorResponseActionResult))]
        public async Task<IActionResult> AddPassengerToFlight([FromRoute] string passengerId, [FromRoute] long flightId)
        {
            var operation = await _personService.AddPassengerToFlightAsync(passengerId, flightId);
            if (operation.Succeeded)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }
    }
}