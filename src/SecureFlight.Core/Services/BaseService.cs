﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Core.Services
{
    public class BaseService<TEntity> : IService<TEntity>
        where TEntity : class
    {
        protected readonly IRepository<TEntity> Repository;

        public BaseService(IRepository<TEntity> repository)
        {
            Repository = repository;
        }

        public async Task<OperationResult<IReadOnlyList<TEntity>>> GetAllAsync()
        {
            return new OperationResult<IReadOnlyList<TEntity>>(await Repository.GetAllAsync());
        }

        public async Task<OperationResult<IReadOnlyList<TEntity>>> GetByFilterAsync(Expression<Func<TEntity, bool>> where)
        {
            return new OperationResult<IReadOnlyList<TEntity>>(await Repository.GetByFilterAsync(where));
        }
    }
}