﻿using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System.Threading.Tasks;

namespace SecureFlight.Core.Services
{
    /// <summary>
    /// Implements a sample command processor.
    /// </summary>
    public class PassengerService : BaseService<Passenger>, IPassengerService
    {
        public new IPassengerRepository Repository { get; set; }
        public PassengerService(IPassengerRepository repository)
            : base(repository)
        {
            Repository = repository;
        }

        public async Task<OperationResult> AddPassengerToFlightAsync(string passengerId, long flightId)
        {
            return await Repository.AddPassengerToFlightAsync(passengerId, flightId);
        }
    }
}