using System.Threading.Tasks;
using SecureFlight.Core.Entities;

namespace SecureFlight.Core.Interfaces
{
    public interface IPassengerRepository : IRepository<Passenger>
    {
        Task<OperationResult> AddPassengerToFlightAsync(string passegerId, long flightId);
    }
}