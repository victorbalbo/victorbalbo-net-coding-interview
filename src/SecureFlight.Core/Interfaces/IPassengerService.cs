﻿using SecureFlight.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SecureFlight.Core.Interfaces
{
    public interface IPassengerService : IService<Passenger>
    {
        Task<OperationResult> AddPassengerToFlightAsync(string passengerId, long flightId);
    }
}