﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SecureFlight.Core.Interfaces
{
    public interface IService<TEntity>
        where TEntity : class
    {
        Task<OperationResult<IReadOnlyList<TEntity>>> GetAllAsync();

        Task<OperationResult<IReadOnlyList<TEntity>>> GetByFilterAsync(Expression<Func<TEntity, bool>> where);
    }
}