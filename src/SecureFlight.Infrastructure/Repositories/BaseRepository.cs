﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Infrastructure.Repositories
{
    public class BaseRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        protected readonly SecureFlightDbContext Context;

        public BaseRepository(SecureFlightDbContext context)
        {
            Context = context;
        }

        public async Task<IReadOnlyList<TEntity>> GetAllAsync()
        {
            return await Context.Set<TEntity>().ToListAsync();
        }

        public async Task<IReadOnlyList<TEntity>> GetByFilterAsync(Expression<Func<TEntity, bool>> where)
        {
            return await Context.Set<TEntity>().Where(where).ToListAsync();
        }

        public TEntity Update(TEntity entity)
        {
            var entry = Context.Entry(entity);
            entry.State = EntityState.Modified;
            Context.SaveChanges();
            return entity;
        }
    }
}