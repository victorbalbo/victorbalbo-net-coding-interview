using System;
using System.Threading.Tasks;
using SecureFlight.Core;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Infrastructure.Repositories
{
    public class PassengerRepository : BaseRepository<Passenger>, IPassengerRepository
    {
        public PassengerRepository(SecureFlightDbContext context) : base(context)
        {
        }

        public async Task<OperationResult> AddPassengerToFlightAsync(string passengerId, long flightId)
        {
            var passengerEntry = await Context.Passengers.FindAsync(passengerId);
            var flightEntry = await Context.Flights.FindAsync(flightId);
            if (passengerEntry == null || flightEntry == null)
            {
                var error = new Error()
                {
                    Code = ErrorCode.InternalError,
                    Message = "Passenger or Flight not found",
                };
                return new OperationResult(error);
            }
            passengerEntry.Flights.Add(flightEntry);
            await Context.SaveChangesAsync();
            return new OperationResult(true); 
        }
    }
}