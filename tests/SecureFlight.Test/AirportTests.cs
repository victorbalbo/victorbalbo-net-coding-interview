using Microsoft.EntityFrameworkCore;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using SecureFlight.Infrastructure;
using SecureFlight.Infrastructure.Repositories;
using System;
using System.Linq;
using Xunit;

namespace SecureFlight.Test
{
    public class AirportTests
    {
        public readonly SecureFlightDbContext _context;
        public readonly IRepository<Airport> _repository;

        public AirportTests()
        {
            var options = new DbContextOptionsBuilder<SecureFlightDbContext>()
                .UseInMemoryDatabase("SecureFlightTest")
                .Options;
            _context = new SecureFlightDbContext(options);
            _repository = new BaseRepository<Airport>(_context);

            _context.Database.EnsureCreated();
        }

        [Fact]
        public void Update_Succeeds()
        {
            var airportCode = "AAQ";
            var changeCountry = "Brazil";
            _repository.Update(new Airport
            {
                City = "Anapa",
                Code = airportCode,
                Country = changeCountry,
                Name = "Anapa Vityazevo"
            });

            CleanDBReference();

            var dbAirport = _context.Find<Airport>(airportCode);

            Assert.Equal(changeCountry, dbAirport.Country);
        }

        private void CleanDBReference()
        {
            _context
                .ChangeTracker
                .Entries()
                .ToList()
                .ForEach(e => e.State = EntityState.Detached);
        }
    }
}